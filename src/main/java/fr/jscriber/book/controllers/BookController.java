package fr.jscriber.book.controllers;

import fr.jscriber.book.dtos.BookDto;
import fr.jscriber.book.dtos.CreateBookDto;
import fr.jscriber.book.entities.Author;
import fr.jscriber.book.entities.Book;
import fr.jscriber.book.exceptions.AuthorNotFoundException;
import fr.jscriber.book.exceptions.BookNotFoundException;
import fr.jscriber.book.repositories.AuthorRepository;
import fr.jscriber.book.repositories.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("book")
public class BookController {

    @Autowired
    private BookRepository repository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<BookDto> getAll() {
        return this.repository.findAll()
                .stream()
                .map((book) -> modelMapper.map(book, BookDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public BookDto getOne(@PathVariable("id") long id) {
        return this.repository.findById(id)
                .map((book) -> modelMapper.map(book, BookDto.class))
                .orElseThrow(() -> new BookNotFoundException(id));
    }

    @DeleteMapping("{id}")
    public void deleteOne(@PathVariable("id") long id) {
        Book book = this.repository.findById(id)
                .orElseThrow(() -> new BookNotFoundException(id));

        this.repository.delete(book);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public BookDto createOne(@RequestBody CreateBookDto dto) {
        Book book = this.modelMapper.map(dto, Book.class);
        Author author = this.authorRepository.findById(dto.getAuthorId())
                .orElseThrow(() -> new AuthorNotFoundException(dto.getAuthorId()));

        book.setId(null);
        book.setAuthor(author);

        return modelMapper.map(this.repository.save(book), BookDto.class);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public BookDto updateOne(@PathVariable("id") long id, @RequestBody CreateBookDto dto) {
        Book book = this.repository.findById(id)
                .orElseThrow(() -> new BookNotFoundException(id));
        Author author = this.authorRepository.findById(dto.getAuthorId())
                .orElseThrow(() -> new AuthorNotFoundException(dto.getAuthorId()));

        this.modelMapper.map(dto, book);
        book.setAuthor(author);

        return modelMapper.map(this.repository.save(book), BookDto.class);
    }
}
