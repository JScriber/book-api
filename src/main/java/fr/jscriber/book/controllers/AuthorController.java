package fr.jscriber.book.controllers;

import fr.jscriber.book.dtos.AuthorDto;
import fr.jscriber.book.dtos.CreateAuthorDto;
import fr.jscriber.book.entities.Author;
import fr.jscriber.book.exceptions.AuthorNotFoundException;
import fr.jscriber.book.repositories.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("author")
public class AuthorController {

    @Autowired
    private AuthorRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<AuthorDto> getAll() {
        return this.repository.findAll()
                .stream()
                .map(author -> modelMapper.map(author, AuthorDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public AuthorDto getOne(@PathVariable("id") long id) {
        return this.repository.findById(id)
                .map(author -> modelMapper.map(author, AuthorDto.class))
                .orElseThrow(() -> new AuthorNotFoundException(id));
    }

    @DeleteMapping("{id}")
    public void deleteOne(@PathVariable("id") long id) {
        Author author = this.repository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id));

        this.repository.delete(author);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public AuthorDto createOne(@RequestBody CreateAuthorDto dto) {
        Author author = this.modelMapper.map(dto, Author.class);

        return modelMapper.map(this.repository.save(author), AuthorDto.class);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public AuthorDto updateOne(@PathVariable("id") long id, @RequestBody CreateAuthorDto dto) {
        Author author = this.repository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id));

        this.modelMapper.map(dto, author);

        return modelMapper.map(this.repository.save(author), AuthorDto.class);
    }

}
