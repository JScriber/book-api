package fr.jscriber.book.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthorNotFoundException extends ResponseStatusException {

    public AuthorNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, String.format("Author %d not found.", id));
    }
}
