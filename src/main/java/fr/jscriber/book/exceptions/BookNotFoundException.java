package fr.jscriber.book.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class BookNotFoundException extends ResponseStatusException {

    public BookNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, String.format("Book %d not found.", id));
    }
}
