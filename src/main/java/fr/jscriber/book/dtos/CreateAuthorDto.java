package fr.jscriber.book.dtos;

import lombok.Data;

@Data
public class CreateAuthorDto {

    private String name;

}
