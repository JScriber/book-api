package fr.jscriber.book.dtos;

import lombok.Data;

import java.util.Set;

@Data
public class AuthorDto {

    private Long id;

    private String name;

}
