package fr.jscriber.book.dtos;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class BookDto {

    private Long id;

    private String name;

    private ZonedDateTime publishDate;

    private Long authorId;

}
