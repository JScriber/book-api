package fr.jscriber.book.dtos;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class CreateBookDto {

    private String name;

    private Long authorId;

    private ZonedDateTime publishDate;

}
