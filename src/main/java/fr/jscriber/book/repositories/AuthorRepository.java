package fr.jscriber.book.repositories;

import fr.jscriber.book.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {

}
